VMG cloud
===

File storage, transcoder jobs and other work with media files

One storage - few buckets and pipelines. This project helps to generate data for array of storages.

Regions
===

 * US Standard
 * us-east-1
 * US West (Oregon) region
 * us-west-2
 * US West (Northern California) region
 * us-west-1
 * EU (Ireland) region
 * eu-west-1
 * EU (Frankfurt) Region
 * eu-central-1
 * Asia Pacific (Singapore) Region
 * ap-southeast-1
 * Asia Pacific (Tokyo) region
 * ap-northeast-1
 * Asia Pacific (Sydney) Region
 * ap-southeast-2
 * South America (Sao Paulo) Region
 * sa-east-1


Container formats
===

3gp, aac, asf, avi, divx, flv, m4a, mkv, mov, mp3, mp4, mpeg, mpeg-ps, mpeg-ts, mxf, ogg, vob, wav, webm


Job statuses
===

Job:Status and Outputs:Status for all of the outputs is Submitted until Elastic Transcoder starts to process the first output.
 * When Elastic Transcoder starts to process the first output, Outputs:Status for that output and Job:Status both change to Progressing. For each output, the value of Outputs:Status remains Submitted until Elastic Transcoder starts to process the output.
 * Job:Status remains Progressing until all of the outputs reach a terminal status, either Complete or Error.
 * When all of the outputs reach a terminal status, Job:Status changes to Complete only if Outputs:Status for all of the outputs is Complete. If Outputs:Status for one or more outputs is Error, the terminal status for Job:Status is also Error.
 * The value of Status is one of the following: Submitted, Progressing, Complete, Canceled, or Error.
